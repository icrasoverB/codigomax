<?php
/*
Template Name: Página publicidad
*/


get_header(); ?>

<div class="wrap contenedor">

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

			<div class="p728x90">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- JF_adaptable -->
				<ins class="adsbygoogle"
				     style="display:block"
				     data-ad-client="ca-pub-7467403016811498"
				     data-ad-slot="6747883566"
				     data-ad-format="auto"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'front-page' );

			?>

				<div class="p728x90">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- JF_320x100 -->
					<ins class="adsbygoogle"
					     style="display:inline-block;width:320px;height:100px"
					     data-ad-client="ca-pub-7467403016811498"
					     data-ad-slot="9701349968"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>

				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation( array(
					'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				) );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->


	<aside id="secondary" class="widget-area" role="complementary">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- JF_320x100 -->
      <ins class="adsbygoogle"
           style="display:inline-block;width:320px;height:100px"
           data-ad-client="ca-pub-7467403016811498"
           data-ad-slot="9701349968"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>

      <br>

      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- JF_300x600 -->
      <ins class="adsbygoogle"
           style="display:inline-block;width:300px;height:600px"
           data-ad-client="ca-pub-7467403016811498"
           data-ad-slot="9472520766"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
	</aside>
</div><!-- .wrap -->

<?php get_footer();
