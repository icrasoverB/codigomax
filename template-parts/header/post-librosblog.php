<div class="wrap">
    <div class="seccion_noticias">
        <h2 class="titular">Libro Blog</h2>
        <article class="post_noticia">
          <?php
              $args = array(
                'posts_per_page' => 1,
                'cat' => 50,
                'post__not_in' => cmax_getNotIn()
              );
              $linkcategoria = '';
              query_posts($args);

              if (have_posts()) :
                while (have_posts()) :
                  the_post();
                  $categories = get_the_category();
                  $categoria = esc_html( $categories[1]->name );
                  $linkcategoria = $categories[0]->term_id;
                  $post_id = get_the_id();
                  cmax_addNotIn($post_id);
                  $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-450', true);
          ?>

          <div class="media-meta">
              <figure class="postimage">
                 <a href="<?php echo esc_url( get_permalink() );?>">
                   <img src="<?php echo $thumb_url[0];?>" alt="<?php the_title();?>">
                </a>
              </figure>
              <div class="entry-info">
                   <p class="clearfix">
                      <span class="meta-item meta-item-label"><a href="#"><?=$categoria;?></a></span>

                     <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                     <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                      <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>


                     </p>

              </div><!-- entry-info -->
          </div>
          <h2 class="title"><a href="<?php echo esc_url( get_permalink() );?>"><?php the_title();?></a></h2>
          <div class="clearfix"></div>

          <?php
             endwhile;
               cmax_reset_query();
             endif;
           ?>
        </article>

        <article class="post_noticia">
          <?php
          $args = array(
            'posts_per_page' => 1,
            'cat' => 50,
            'post__not_in' => cmax_getNotIn()
          );
              query_posts($args);

              if (have_posts()) :
                while (have_posts()) :
                  the_post();
                  $categories = get_the_category();
                  $categoria = esc_html( $categories[1]->name );
                  $linkcategoria = $categories[0]->term_id;
                  $post_id = get_the_id();
                  cmax_addNotIn($post_id);
                  $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-450', true);
          ?>

          <div class="media-meta">
              <figure class="postimage">
                 <a href="<?php echo esc_url( get_permalink() );?>">
                   <img src="<?php echo $thumb_url[0];?>" alt="<?php the_title();?>">
                </a>
              </figure>
              <div class="entry-info">
                   <p class="clearfix">
                      <span class="meta-item meta-item-label"><a href="#"><?=$categoria;?></a></span>

                     <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                     <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                     <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
                     </p>

              </div><!-- entry-info -->
          </div>
          <h2 class="title"><a href="<?php echo esc_url( get_permalink() );?>"><?php the_title();?></a></h2>
          <div class="clearfix"></div>

          <?php
             endwhile;
               cmax_reset_query();
             endif;
           ?>
        </article>

        <article class="post_noticia">
          <?php
          $args = array(
            'posts_per_page' => 1,
            'cat' => 50,
            'post__not_in' => cmax_getNotIn()
          );
              query_posts($args);

              if (have_posts()) :
                while (have_posts()) :
                  the_post();
                  $categories = get_the_category();
                  $categoria = esc_html( $categories[1]->name );
                  $linkcategoria = $categories[0]->term_id;
                  $post_id = get_the_id();
                  cmax_addNotIn($post_id);
                  $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-450', true);
          ?>

          <div class="media-meta">
              <figure class="postimage">
                 <a href="<?php echo esc_url( get_permalink() );?>">
                   <img src="<?php echo $thumb_url[0];?>" alt="<?php the_title();?>">
                </a>
              </figure>
              <div class="entry-info">
                   <p class="clearfix">
                    <span class="meta-item meta-item-label"><a href="#"><?=$categoria;?></a></span>
                     <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                     <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                     <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
                     </p>

              </div><!-- entry-info -->
          </div>
          <h2 class="title"><a href="<?php echo esc_url( get_permalink() );?>"><?php the_title();?></a></h2>
          <div class="clearfix"></div>

          <?php
             endwhile;
               cmax_reset_query();
             endif;
           ?>
        </article>


        <div class="text-center more">
			     <a class="button" href="<?php echo get_category_link( $linkcategoria );?>">MOSTRAR MAS..</a>
		     </div>

    </div>
</div>
