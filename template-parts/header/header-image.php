<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<div class="nav-menu-fix">
		<div class="fondo-fix">
			<div class="wrap">
				<div class="c-nav-bar">
					<div class="table-cell-top">
						<div class="logo-empresarial">
							<a href="/">
								<img class="logo-max" src="<?php echo get_template_directory_uri();?>/assets/images/new_logo.svg">
							</a>
						</div>
						<div class="relative inline-block fr-movil">
							<a class="link-menu-fix border-left border-right">
								<div class="t-btn-context-nav">
									<div class="inline-block">Categorias</div>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</div>
							</a>
							<div class="link-submenu-fix">
							<?php wp_nav_menu( array(
								'theme_location' => 'top',
								'menu_id'        => 'top-menu',
							) ); ?>
									<!--<ul>
										<li><a href="#" class="t-context-nav">Tecnologia</a></li>
										<li><a href="#" class="t-context-nav">Tecnologia</a></li>
										<li><a href="#" class="t-context-nav">Tecnologia</a></li>
										<li><a href="#" class="t-context-nav">Tecnologia</a></li>
										<li><a href="#" class="t-context-nav">Tecnologia</a></li>
									</ul>-->
								</div>
						</div>
						
					</div>
					<div class="o-bar__cell m-oculta">
						<div class="social-redes">
							<?php dynamic_sidebar( 'sidebar-7' ); ?>
						</div>
					</div>
				</div>
				
			</div>
		</div>
</div>


<div class="custom-header m-oculta">

	<div class="logo-empresa m-oculta">
		<div class="wrap">
			<a href="/">
				<img class="logo-max" src="<?php echo get_template_directory_uri();?>/assets/images/logo_black.svg">
			</a>
		</div>
	</div>

	<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>

</div><!-- .custom-header -->
