
<?php if(is_home()){ ?>



<section class="secciones_component">
		<div class="header-seccion th-fondo">
			<div class="wrap">
    			<div class="table-meta">
					<div class="o-title_mark table-cell-bottom">
						<h2 class="th-title t-section-title mt-15 pb-0">
							Lo ultimo en  Salud
						</h2>		
					</div>
				</div>
    		</div>
		  </div>
		  <div class="relative">
			  <div class="o-backdrop bg-gray-1"></div>
			  <div class="relative wrap">
					<section class="grid">

							<div class="grid-cell">

                <?php
                    $args = array(
                      'posts_per_page' => 1,
                      'cat' => 5,
                      'post__not_in' => cmax_getNotIn()
                    );
                    $linkcategoria = '';
                    query_posts($args);

                    if (have_posts()) :
                      while (have_posts()) :
                        the_post();
                        $categories = get_the_category();
                        $categoria = esc_html( $categories[0]->name );
                        $rl_category_color = rl_color($categories[0]->cat_ID);
                        $linkcategoria = $categories[0]->term_id;
                        $post_id = get_the_id();
                        cmax_addNotIn($post_id);
                        $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-385', true);
                ?>

								<article>
									<div class="o-hit pb-10">
										<div class="o-rating_thumb">
											<img alt="<?php the_title();?>" class="stretch-img" width="385" height="257" src="<?php echo $thumb_url[0];?>" style="display: block;">
										</div>
										<div class="mt-25">
											<div class="t-meta hide@m-">
												<a href="#" class="th-topic">
											  	<?php 
                            $tag = get_the_tags();
                            echo $tag[0]->name; 
                          ?>
												</a>
											</div>

											<div class="th-title">
												<h2 class="t-h3">
												<span class="th-underline">
													<?php the_title_shorten(55,'...');?>
												</span>
												</h2>
											</div>
											<div class="mt-10-m">
												<p class="texto-grid"><?php echo get_excerpt(55);?></p>
											</div>
											<div class="mt-5-m">
												<div class="content-entry-meta">
													<?php the_author(); ?> •
													
													<i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.
												</div>
											</div>

										</div>
										<a href="<?php echo esc_url( get_permalink() );?>" class="o-hit__link">View</a>
                  </div>
                  
                  <?php
                    endwhile;
                      cmax_reset_query();
                    endif;
                  ?>
								</article>	
								
              </div>
              
							<div class="grid-cell grid-divider">

                 <?php
                    $args = array(
                      'posts_per_page' => 1,
                      'cat' => 5,
                      'post__not_in' => cmax_getNotIn()
                    );
                    $linkcategoria = '';
                    query_posts($args);

                    if (have_posts()) :
                      while (have_posts()) :
                        the_post();
                        $categories = get_the_category();
                        $categoria = esc_html( $categories[0]->name );
                        $rl_category_color = rl_color($categories[0]->cat_ID);
                        $linkcategoria = $categories[0]->term_id;
                        $post_id = get_the_id();
                        cmax_addNotIn($post_id);
                        $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-385', true);
                ?>

								<article>
									<div class="o-hit pb-10">
										<div class="o-rating_thumb">
											<img alt="<?php the_title();?>" class="stretch-img" width="385" height="257" src="<?php echo $thumb_url[0];?>" style="display: block;">
										</div>
										<div class="mt-25">
											<div class="t-meta hide@m-">
												<a href="#" class="th-topic">
											  	<?php 
                            $tag = get_the_tags();
                            echo $tag[0]->name; 
                          ?>
												</a>
											</div>

											<div class="th-title">
												<h2 class="t-h3">
												<span class="th-underline">
													<?php the_title_shorten(55,'...');?>
												</span>
												</h2>
											</div>
											<div class="mt-10-m">
												<p class="texto-grid"><?php echo get_excerpt(55);?></p>
											</div>
											<div class="mt-5-m">
												<div class="content-entry-meta">
													<?php the_author(); ?> •
													
													<i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.
												</div>
											</div>

										</div>
										<a href="<?php echo esc_url( get_permalink() );?>" class="o-hit__link">View</a>
                  </div>
                  
                  <?php
                    endwhile;
                      cmax_reset_query();
                    endif;
                  ?>
								</article>	
                
              </div>
              
							<div class="grid-cell grid-divider">
								
								<?php
                    $args = array(
                      'posts_per_page' => 1,
                      'cat' => 5,
                      'post__not_in' => cmax_getNotIn()
                    );
                    $linkcategoria = '';
                    query_posts($args);

                    if (have_posts()) :
                      while (have_posts()) :
                        the_post();
                        $categories = get_the_category();
                        $categoria = esc_html( $categories[0]->name );
                        $rl_category_color = rl_color($categories[0]->cat_ID);
                        $linkcategoria = $categories[0]->term_id;
                        $post_id = get_the_id();
                        cmax_addNotIn($post_id);
                        $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-385', true);
                ?>

								<article>
									<div class="o-hit pb-10">
										<div class="o-rating_thumb">
											<img alt="<?php the_title();?>" class="stretch-img" width="385" height="257" src="<?php echo $thumb_url[0];?>" style="display: block;">
										</div>
										<div class="mt-25">
											<div class="t-meta hide@m-">
												<a href="#" class="th-topic">
											  	<?php 
                            $tag = get_the_tags();
                            echo $tag[0]->name; 
                          ?>
												</a>
											</div>

											<div class="th-title">
												<h2 class="t-h3">
												<span class="th-underline">
													<?php the_title_shorten(55,'...');?>
												</span>
												</h2>
											</div>
											<div class="mt-10-m">
												<p class="texto-grid"><?php echo get_excerpt(55);?></p>
											</div>
											<div class="mt-5-m">
												<div class="content-entry-meta">
													<?php the_author(); ?> •
													
													<i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.
												</div>
											</div>

										</div>
										<a href="<?php echo esc_url( get_permalink() );?>" class="o-hit__link">View</a>
                  </div>
                  
                  <?php
                    endwhile;
                      cmax_reset_query();
                    endif;
                  ?>
                </article>	
                
              </div>
              
					</section>
			  </div>
		  </div>
		
</section>

<?php }?>
