<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>






<div class="site-branding">
	<div class="wrap">
		<a href="/">
				<img class="logo-max movil-logo" src="<?php echo get_template_directory_uri();?>/assets/images/new_logo.svg">
			</a>
		<div class="site-branding-text">
			
			<?php if ( has_nav_menu( 'top' ) ) : ?>
				<div class="navigation-top-primario">

						<?php get_template_part( 'template-parts/navigation/navigation', 'top-primario' ); ?>

						<div class="buscador">
							<?php echo twentyseventeen_get_svg( array( 'icon' => 'search' ) ); ?>
							<?php echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );?>
						</div>

						<div class="social-redes">
							<?php dynamic_sidebar( 'sidebar-7' ); ?>
						</div>
						

						<div class="buscador-personalizado-desktop">
	            <script>
	              (function() {
	                var cx = '008023784961996014673:msrbtdq3ltg';
	                var gcse = document.createElement('script');
	                gcse.type = 'text/javascript';
	                gcse.async = true;
	                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
	                var s = document.getElementsByTagName('script')[0];
	                s.parentNode.insertBefore(gcse, s);
	              })();
	            </script>
	            <gcse:search></gcse:search>
	          </div>
						<script type="text/javascript">
							jQuery(function(){
								var search = 0;
								jQuery('.buscador .icon-close').hide();
								jQuery(document).on('click', '.buscador', function(){
									if(search==0){
										jQuery(this).addClass('bg-buscador-cerrar');
										jQuery('.buscador .icon-search').hide();
										jQuery('.buscador .icon-close').show();
										jQuery('.buscador-personalizado-desktop').fadeIn(500);
										search = 1;
									}
									else{
										jQuery(this).removeClass('bg-buscador-cerrar');
										jQuery('.buscador .icon-close').hide();
										jQuery('.buscador .icon-search').show();
										jQuery('.buscador-personalizado-desktop').fadeOut(500);
										search = 0;
									}
								});
								var subm = 0;
								jQuery(document).on('click', '.link-menu-fix', function(){
									if(subm==0){
										jQuery(this).addClass('fondo-gray');
										jQuery('.link-submenu-fix').addClass('mostrar_submenu');
										subm = 1;
									}
									else{
										jQuery(this).removeClass('fondo-gray');
										jQuery('.link-submenu-fix').removeClass('mostrar_submenu');
										subm = 0;
									}
								});

								if(!jQuery.browser.mobile){
									jQuery(window).scroll(function () {
							        if (jQuery(this).scrollTop() > 100) {
							           jQuery('.nav-menu-fix').addClass( 'mostrar-menu-fix' );
							        } else {
							            jQuery('.nav-menu-fix').removeClass( 'mostrar-menu-fix' );
							        }
							    });
								}

							});
						</script>

				</div>
			<?php endif; ?>

		</div>

		<?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && ! has_nav_menu( 'top' ) ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
		<?php endif; ?>

	</div><!-- .wrap -->
</div><!-- .site-branding -->
