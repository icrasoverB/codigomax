
  <?php 

    $categories = get_the_category();
$category_id = $categories[0]->cat_ID;
?>

	<div class="wrap" id="featured">

 				<div class="col-mid">

 				<article>

 					<?php
 							$args = array(
 								'posts_per_page' => 1,
 								'post__not_in' => cmax_getNotIn(),
								'category__in' => array($category_id)
 							);
 							query_posts($args);

 							if (have_posts()) :
 								while (have_posts()) :
 									the_post();
 									$post_id = get_the_id();
 									cmax_addNotIn($post_id);

									$categories = get_the_category();
									if ( ! empty( $categories ) ) {
									  $categoria = esc_html( $categories[0]->name );
									  $rl_category_color = rl_color($categories[0]->cat_ID);
									}
									else{
									  $categoria = 'Sin definir';
									}


 									$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);
 					?>
 							<a href="<?php echo esc_url( get_permalink() );?>">
 									<figure style="background:url('<?php echo $thumb_url[0];?> ')">
 											<div class="overgrad combo"></div>
											<span class="shadow"></span>
 												<h3 class="title"><?php the_title();?></h3>
 									</figure>
 							</a>
							<div class="category">
								<div class="entry-info">
                     <p class="clearfix">
                        <span class="meta-item meta-item-label" style="background-color:<?=$rl_category_color;?> !important;"><a href="<?php echo get_category_link( $categories[0]->term_id );?>"><?=$categoria;?></a></span>
                       <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                       <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                        <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
                       </p>
                </div><!-- entry-info -->
 						</div>
 					<?php
 						endwhile;
 							cmax_reset_query();
 						endif;
 					?>

 				</article>



 		</div>

 		<div class="col-one">



 				<article>
 					<?php
 							$args = array(
 								'posts_per_page' => 1,
 								'post__not_in' => cmax_getNotIn(),
								'category__in' => array($category_id)
 							);
 							query_posts($args);

 							if (have_posts()) :
 								while (have_posts()) :
 									the_post();
 									$post_id = get_the_id();
 									cmax_addNotIn($post_id);

									$categories = get_the_category();
									if ( ! empty( $categories ) ) {
									  $categoria = esc_html( $categories[0]->name );
									  $rl_category_color = rl_color($categories[0]->cat_ID);
									}
									else{
									  $categoria = 'Sin definir';
									}

 									$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);
 					?>
 							<a href="<?php echo esc_url( get_permalink() );?>">
 									<figure style="background:url('<?php echo $thumb_url[0];?> ')">
 											<div class="overgrad combo"></div>
											<span class="shadow"></span>
 												<h3 class="title"><?php the_title();?></h3>
 									</figure>
 							</a>

							<div class="category">
								<div class="entry-info">
                     <p class="clearfix">
                        <span class="meta-item meta-item-label" style="background-color:<?=$rl_category_color;?> !important;"><a href="<?php echo get_category_link( $categories[0]->term_id );?>"><?=$categoria;?></a></span>
                       <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                       <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                        <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
                       </p>
                </div><!-- entry-info -->
 						</div>
 					<?php
 						endwhile;
 							cmax_reset_query();
 						endif;
 					?>
 				</article>



 				<article>
 					<?php
 							$args = array(
 								'posts_per_page' => 1,
 								'post__not_in' => cmax_getNotIn(),
								'category__in' => array($category_id)
 							);
 							query_posts($args);

 							if (have_posts()) :
 								while (have_posts()) :
 									the_post();
 									$post_id = get_the_id();
 									cmax_addNotIn($post_id);
 									$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);

									$categories = get_the_category();
									if ( ! empty( $categories ) ) {
									  $categoria = esc_html( $categories[0]->name );
									  $rl_category_color = rl_color($categories[0]->cat_ID);
									}
									else{
									  $categoria = 'Sin definir';
									}
 					?>
 							<a href="<?php echo esc_url( get_permalink() );?>">
 									<figure style="background:url('<?php echo $thumb_url[0];?> ')">
 											<div class="overgrad combo"></div>
											<span class="shadow"></span>
 												<h3 class="title"><?php the_title();?></h3>
 									</figure>
 							</a>
							<div class="category">
								<div class="entry-info">
                     <p class="clearfix">
                        <span class="meta-item meta-item-label" style="background-color:<?=$rl_category_color;?> !important;"><a href="<?php echo get_category_link( $categories[0]->term_id );?>"><?=$categoria;?></a></span>
                       <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
                       <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
                        <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
                       </p>
                </div><!-- entry-info -->
 						</div>
 					<?php
 						endwhile;
 							cmax_reset_query();
 						endif;
 					?>
 				</article>





 		</div>


 		<div class="col-two">


 			 <article>
 				 <?php
 						 $args = array(
 							 'posts_per_page' => 1,
 							 'post__not_in' => cmax_getNotIn(),
							 'category__in' => array($category_id)
 						 );
 						 query_posts($args);

 						 if (have_posts()) :
 							 while (have_posts()) :
 								 the_post();
 								 $post_id = get_the_id();
 								 cmax_addNotIn($post_id);
 								 $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);

								 $categories = get_the_category();
								 if ( ! empty( $categories ) ) {
									 $categoria = esc_html( $categories[0]->name );
									 $rl_category_color = rl_color($categories[0]->cat_ID);
								 }
								 else{
									 $categoria = 'Sin definir';
								 }
 				 ?>
 						 <a href="<?php echo esc_url( get_permalink() );?>">
 								 <figure style="background:url('<?php echo $thumb_url[0];?> ')">
 										 <div class="overgrad combo"></div>
										 <span class="shadow"></span>
 											 <h3 class="title"><?php the_title();?></h3>
 								 </figure>
 						 </a>
						 <div class="category">
							 <div class="entry-info">
										<p class="clearfix">
											 <span class="meta-item meta-item-label" style="background-color:<?=$rl_category_color;?> !important;"><a href="<?php echo get_category_link( $categories[0]->term_id );?>"><?=$categoria;?></a></span>
											<span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
											<span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
											 <span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
											</p>
							 </div><!-- entry-info -->
					 </div>
 				 	<?php
 					 endwhile;
 						 cmax_reset_query();
 					 endif;
 				 ?>
 				</article>

 			 <article>
 				 <?php
 						 $args = array(
 							 'posts_per_page' => 1,
 							 'post__not_in' => cmax_getNotIn(),
							 'category__in' => array($category_id)
 						 );
 						 query_posts($args);

 						 if (have_posts()) :
 							 while (have_posts()) :
 								 the_post();
 								 $post_id = get_the_id();
 								 cmax_addNotIn($post_id);
 								 $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);

								 $categories = get_the_category();
								 if ( ! empty( $categories ) ) {
									 $categoria = esc_html( $categories[0]->name );
									 $rl_category_color = rl_color($categories[0]->cat_ID);
								 }
								 else{
									 $categoria = 'Sin definir';
								 }
 				 ?>
 						 <a href="<?php echo esc_url( get_permalink() );?>">
 								 <figure style="background:url('<?php echo $thumb_url[0];?> ')">
 										 <div class="overgrad combo"></div>
										 <span class="shadow"></span>
 											 <h3 class="title"><?php the_title();?></h3>
 								 </figure>
 						 </a>
						 <div class="category">
 							<div class="entry-info">
 									 <p class="clearfix">
 											<span class="meta-item meta-item-label" style="background-color:<?=$rl_category_color;?> !important;"><a href="<?php echo get_category_link( $categories[0]->term_id );?>"><?=$categoria;?></a></span>
 										 <span class="meta-item meta-item-author"><a href="#" class="link"><?php the_author(); ?></a></span>
 										 <span class="meta-item meta-item-date"><?php echo get_the_date(); ?></span>
 											<span class="meta-item meta-item-author"><a href="#" class="link"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.</a></span>
 										 </p>
 							</div><!-- entry-info -->
 					</div>
 				 <?php
 					 endwhile;
 						 cmax_reset_query();
 					 endif;
 				 ?>
 				</article>


 		</div>
 </div>
