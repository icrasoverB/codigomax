<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
if($GLOBALS['articulo']%2==0){$clase_articulo='a-left';}else{$clase_articulo='a-right';}
?>

<article id="a-style post-<?php the_ID(); ?> " <?php post_class($clase_articulo); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-thumbnail-750' ); ?>
			</a>
<?php
			$categories = get_the_category();
			if ( ! empty( $categories ) ) {
			    echo "<span class='post-type-category'>".esc_html( $categories[0]->name )."</span>";
			}
			?>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<header class="entry-header">
		<?php

		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}

		if ( 'post' === get_post_type() ) {
			echo '<div class="entry-meta">';

				if ( is_single() ) {
					twentyseventeen_posted_on();
				} else {
					/*echo twentyseventeen_time_link();*/
					echo '<a href="'.esc_url( get_permalink() ).'" rel="bookmark"><i class="fa fa-clock-o" aria-hidden="true"></i> '.get_the_date();
					echo ' | <i class="fa fa-eye" aria-hidden="true"></i> '.getPostViews(get_the_ID()).' Visitas.</a>';
					twentyseventeen_edit_link();
				};
			echo '</div>';
		};
		?>
	</header><!-- .entry-header -->



	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		$post_content = the_excerpt();

		echo $post_content;



		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>

	</div><!-- .entry-content -->
	<?php
		if (is_home() ) :
	 ?>
		<!--<a href="<?php echo esc_url(get_permalink() );?>" class="more-link">Seguir leyendo</a>-->
		<?php the_tags( '<ul class="lis-tag"><li>', '</li><li>', '</li></ul>' ); ?>
	<?php
		endif;
	 ?>
	<?php
	if ( is_single() ) {
		twentyseventeen_entry_footer();
	}
	?>

</article><!-- #post-## -->
