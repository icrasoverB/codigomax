<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', true);

$categories = get_the_category();
if ( ! empty( $categories ) ) {
  $categoria = esc_html( $categories[0]->name );
  $rl_category_color = rl_color($categories[0]->cat_ID);
}
else{
  $categoria = 'Sin definir';
}
?>

<div class="articulo-post post-<?php the_ID(); ?>">
    <div class="post-imagen-articulo" style="background-image:url('<?php echo $thumb_url[0];?>');">
      <a href="<?php echo esc_url( get_permalink() );?>" rel="bookmark" class="overlay-post">
          <span><i class="fa fa-caret-right"></i> Leer articulo</span>
      </a>
    </div>
    <div class="content-articulo">
        <a href="<?php echo esc_url( get_permalink() );?>"><h4><?php the_title();?></h4></a>
        <div class="content-entry">
            <?php the_author(); ?> •
            <i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Visitas.
        </div>
    </div>
    <a class="category-articulo" style="background:<?=$rl_category_color;?>;" href="<?php echo get_category_link( $categories[0]->term_id );?>"><?php echo $categoria;?></a>
</div>
