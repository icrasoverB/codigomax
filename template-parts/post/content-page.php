<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<?php

	setPostViews(get_the_ID());

	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;

	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) )  ) :

		$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'twentyseventeen-thumbnail-750', true);
		echo '<div class="codigo-single-bg" style="background-image: url('.$thumb_url[0].');">';

		?>
		<div class="over-box"></div>
		<header class="entry-header">
				<?php

				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}

				if ( 'post' === get_post_type() ) {
					echo '<div class="entry-meta">';
						if ( is_single() ) {
							twentyseventeen_posted_on();
						} else {
							echo twentyseventeen_time_link();
							twentyseventeen_edit_link();
						};
					echo '<span class="entry-meta"> - <i class="fa fa-eye" aria-hidden="true"></i> '.getPostViews(get_the_ID()).' Visitas.';

					echo '</div>';
				};
				?>
		</header>



		<?php
		echo '</div>';
	endif;

	?>



	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="p728x90">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- CM_ADAPTABLE -->
		<ins class="adsbygoogle"
			style="display:block"
			data-ad-client="ca-pub-8763209228159961"
			data-ad-slot="1340181614"
			data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
	<br>
	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Leer mas<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->



</article><!-- #post-## -->
