<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="/">
		<img class="logo-max-footer" src="<?php echo get_template_directory_uri();?>/assets/images/new_logo.svg">
	</a>
	<!--<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a>-->
	<p>DERECHOS RESERVADO - CODIGOMAX.COM @2017  <a class="link-naranja" href="<?php echo home_url();?>/termino-y-condiciones">TERMINOS</a> | <a class="link-naranja" href="<?php echo home_url();?>/cookie-policy">POLITICA</a></p>
</div><!-- .site-info -->
