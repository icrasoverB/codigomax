<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap bg_white">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<br>
			<div class="contenido-post-articulos">
			<?php
			$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
			$args = array(
				'post__not_in' => cmax_getNotIn(),
				'category__not_in' => array(5),
				'paged'	=> $paged
			);
			query_posts($args);

			if ( have_posts() ) :
			$i = 1;
			while ( have_posts() ) : the_post();
					$post_id = get_the_id();
					cmax_addNotIn($post_id);

					get_template_part( 'template-parts/post/post', get_post_format());
					if($i==4):
						?>
						
						<div class="p_adaptable mb-10">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<!-- CM_ADAPTABLE -->
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-client="ca-pub-8763209228159961"
								data-ad-slot="1340181614"
								data-ad-format="auto"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>

						<br>
						<?php
					endif;
					$i++;
			endwhile;



			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;

			?>

			</div>

			<?php the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );
			cmax_reset_query();
			?>






		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>



</div><!-- .wrap -->


<?php get_template_part( 'template-parts/header/post', 'salud'); ?>
<?php //get_template_part( 'template-parts/header/post', 'librosblog' ); ?>

<?php get_footer();
