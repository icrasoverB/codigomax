<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap bg_white espacio">
	<div id="primary" class="content-area">
		<main id="main" class="site-main contenedor" role="main">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content', 'page');

				?>

				<div class="p728x90">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- CM_ADAPTABLE -->
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-client="ca-pub-8763209228159961"
						data-ad-slot="1340181614"
						data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>

				<?php

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->


		<?php
			/** post relacionados */
			echo do_shortcode('[post-relacionado]');
		?>

		<div class="comentarios">
				<?php

				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;*/

				 ?>
				 <h3>Deja tu comentario</h3>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=232658346802041";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>

				<div class="fb-comments fb_iframe_widget_fluid" data-href="<?php esc_url( get_permalink() );?>"  data-numposts="5"></div>
		</div>

	</div><!-- #primary -->


	<!--<aside id="secondary" class="widget-area" role="complementary">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

		<ins class="adsbygoogle"
		     style="display:inline-block;width:300px;height:250px"
		     data-ad-client="ca-pub-7467403016811498"
		     data-ad-slot="7945415164"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	</aside>
</div> -->
<?php get_sidebar(); ?>
<?php get_footer();
