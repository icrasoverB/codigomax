<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">

    <div class="caja_suscripcion">
        <h5>Suscribete a nuestro Boletin de Noticias Gratis!</h5>
        <span>Se el primero en recibir nuestra noticia</span>
        <a href="#" class="shownewsletterbox">Suscribirse a la Newsletter</a>

    </div>

    <div class="p_adaptable">
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- CD_300X250 -->
      <ins class="adsbygoogle"
          style="display:inline-block;width:300px;height:250px"
          data-ad-client="ca-pub-8763209228159961"
          data-ad-slot="4735489385"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>

    <?php dynamic_sidebar( 'sidebar-1' ); ?>

    <div class="p_adaptable">
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- CD_300X600 -->
      <ins class="adsbygoogle"
          style="display:inline-block;width:300px;height:600px"
          data-ad-client="ca-pub-8763209228159961"
          data-ad-slot="1196888998"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>

</aside><!-- #secondary -->
