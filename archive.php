<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<?php get_template_part( 'template-parts/header/post', 'archive' ); ?>
<div class="wrap bg_white espacio">
	

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php 
			$categories = get_the_category();
			$name = $categories[0]->name;
			?>
			<h2 class="nav_categoria">Categoria: <?php echo $name;?></h2>
			<div class="p_adaptable">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div>
			<br>

		<?php
		if ( have_posts() ) : ?>
			<?php
			$i = 1;
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/post', get_post_format() );
				if($i==4):
						?>
						
						<div class="p_adaptable">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<!-- CM_ADAPTABLE -->
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-client="ca-pub-8763209228159961"
								data-ad-slot="1340181614"
								data-ad-format="auto"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>

						<br>
						<?php
					endif;
					$i++;

			endwhile;

			the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>

		<div class="p_adaptable">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<!-- CM_ADAPTABLE -->
							<ins class="adsbygoogle"
								style="display:block"
								data-ad-client="ca-pub-8763209228159961"
								data-ad-slot="1340181614"
								data-ad-format="auto"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
		</div>
		<br>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
